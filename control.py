import time
import serial

payload_length = 7
tty = "/dev/ttyS1"
baudrate = 9600
timeout = 0.1

ser = serial.Serial(tty, baudrate, timeout=timeout)
count = 0

payload_0 = bytearray([0xC4, 0x20, 0x78, 0x28, 0x00, 0x00, 0x9A])
payload_1 = bytearray([0xC4, 0x20, 0x78, 0x28, 0x0C, 0x00, 0x91])
payload_2 = bytearray([0xC4, 0x20, 0x78, 0x28, 0x14, 0x00, 0x81])
payload_3 = bytearray([0xC4, 0x20, 0x78, 0xC4, 0x12, 0x00, 0x0D])
payload_4 = bytearray([0xC4, 0x20, 0x78, 0xC4, 0x0D, 0x80, 0x98])
payload_5 = bytearray([0xC4, 0x20, 0x78, 0xC4, 0x0F, 0x00, 0x1A])
payload_6 = bytearray([0xC4, 0x20, 0x78, 0x28, 0x0B, 0x00, 0x94])
payload_7 = bytearray([0xC4, 0x20, 0x78, 0xA4, 0x13, 0x00, 0x4C])
payload_8 = bytearray([0xC4, 0x20, 0x78, 0xC4, 0x07, 0x00, 0x12])

while 1:
	print("Sending the payload")
	ser.write(payload_1)
	response = serial.read(7)
	print("Response {}".format(":".join("{:02x}".format(response))))
	time.sleep(10)
	count += 1
