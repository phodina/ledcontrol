# LED Control
This repo contains captured UART payloads between the Amlogic S905X4 and the LED Ring controller.

# Setup
The communication is 9600 Bd 8n1 UART protocol where there is always command send from the Amlogic S905X4 (Android LED Control apk app) to the microcontroller (ARM Cortex M3) that implements all the patterns.

The protocol is stateless so the LED pattern is always selected by the command. Hoewver, the controller remembers the last state and displays it unless it's commanded otherwise.

The payload is 7 bytes long with prefixes for commands (`0xC4 0x20 0x78`) and responses (`0x23 0x04 0x1E`) followed by some CRC as the last byte.
 
# Sequences
- Off [Rx](X96Ultra_OFF_RX.png) [Tx](X96Ultra_OFF_TX.png)
```
Tx: 0xC4 0x20 0x78 0x28 0x00 0x00 0x9A
Rx: 0x23 0x04 0x1E 0x14 0x00 0x00 0x59
```
- Fixed Display Control 2 [Rx](X96Ultra_Fixed_Display_Color2_RX.png) [Tx](X96Ultra_Fixed_Display_Color2_TX.png)
```
Tx: 0xC4 0x20 0x78 0x28 0x0C 0x00 0x91
Rx: 0x23 0x04 0x1E 0x14 0x30 0x00 0x89
```
- Fixed Display Control 1 [Rx](X96Ultra_Fixed_Display_Color1_RX.png) [Tx](X96Ultra_Fixed_Display_Color1_TX.png)
```
Tx: 0xC4 0x20 0x78 0x28 0x14 0x00 0x81
Rx: 0x23 0x04 0x1E 0x14 0x28 0x00 0x81
```
- Flow Display Automatic [Rx](X96Ultra_Flow_Display_Automatic_RX.png) [Tx](X96Ultra_Flow_Display_Automatic_TX.png)
```
Tx: 0xC4 0x20 0x78 0xC4 0x12 0x00 0x0D
Rx: 0x23 0x04 0x1E 0x23 0x48 0x00 0xB0
```
- Source-to-end running water shows white and red [Rx](X96Ultra_Source_To_End_Running_Water_Shows_White_And_Red_RX.png) [Tx](X96Ultra_Source_To_End_Running_Water_Shows_White_And_Red_TX.png)
```
Tx: 0xC4 0x20 0x78 0xC4 0x0D 0x80 0x98
Rx: 0x23 0x04 0x1E 0x23 0xB0 0x01 0x19
```
- Fade in and fade out automatically [Rx](X96Ultra_Fade_In_and_Fade_Out_Automatically_RX.png) [Tx](X96Ultra_Fade_In_and_Fade_Out_Automatically_TX.png)
```
Tx: 0xC4 0x20 0x78 0xC4 0x0F 0x00 0x1A
Rx: 0x23 0x04 0x1E 0x23 0xF0 0x00 0x58
```
- Source to end display rainbow colors [Rx](X96Ultra_Source_To_End_Display_Rainbow_Colors_RX.png) [Tx](X96Ultra_Source_To_End_Display_Rainbow_Colors_TX.png)
```
Tx: 0xC4 0x20 0x78 0x28 0x0B 0x00 0x94
Rx: 0x23 0x04 0x1E 0x14 0xD0 0x00 0x29
```
- Color flashes automatically [Rx](X96Ultra_Color_Flashes_Automatically_RX.png) [Tx](X96Ultra_Color_Flashes_Automatically_TX.png)
```
Tx: 0xC4 0x20 0x78 0xA4 0x13 0x00 0x4C
Rx: 0x23 0x04 0x1E 0x25 0xC8 0x00 0x32
```
- Fully automatic [Rx](X96Ultra_Fully_Automatic_RX.png) [Tx](X96Ultra_Fully_Automatic_TX.png)
```
Tx: 0xC4 0x20 0x78 0xC4 0x07 0x00 0x12
Rx: 0x23 0x04 0x1E 0x23 0xE0 0x00 0x48
```

# TODO
- [ ] Decode the protocol
- [ ] Write simple Python script to send and receive the payload over serial port
- [ ] Create KODI plugin to control the LED ring
- [ ] Add short videos for individual patterns
- [ ] Fuzz the protocol to check if there are other patterns
- [ ] Decompile the APK and check what's inside
